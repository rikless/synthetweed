<?php

class SynthetweedsController extends BaseController {

    /**
     * Synthetweed Repository
     *
     * @var Synthetweed
     */
    protected $synthetweed;

    public function __construct(Synthetweed $synthetweed)
    {
        $this->synthetweed = $synthetweed;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $synthetweeds = $this->synthetweed->all();

        return View::make('synthetweeds.index', compact('synthetweeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('synthetweeds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        if (Auth::check()) {

            $input['user_id'] = Auth::user()->id;

        }else{

            Redirect::route('users.login');
        }

        $validation = Validator::make($input, Synthetweed::$rules);



        if ($validation->passes())
        {
            $this->synthetweed->create($input);

            return Redirect::route('synthetweeds.myfeed');
        }

        return Redirect::route('synthetweeds.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $synthetweed = $this->synthetweed->findOrFail($id);

        return View::make('synthetweeds.show', compact('synthetweed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $synthetweed = $this->synthetweed->find($id);

        if (is_null($synthetweed))
        {
            return Redirect::route('synthetweeds.index');
        }

        return View::make('synthetweeds.edit', compact('synthetweed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');

        if (Auth::check()) {
            
            $input['user_id'] = Auth::user()->id;

        }

        $validation = Validator::make($input, Synthetweed::$rules);

        if ($validation->passes())
        {
            $synthetweed = $this->synthetweed->find($id);
            $synthetweed->update($input);

            return Redirect::route('synthetweeds.myfeed');
        }

        return Redirect::route('synthetweeds.myfeed', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->synthetweed->find($id)->delete();

        return Redirect::route('synthetweeds.index');
    }

    /**
     * List all tweeds from logged user feed.
     *
     * @return Response
     */
    public function myfeed()
    {

        if (Auth::check()) {

            $data['tweeds'] = Synthetweed::where('user_id', '=', Auth::user()->id)->get();
        
        }else{

            return Redirect::route('users.login');
        }

        return View::make('synthetweeds.myfeed', $data);
    }

}
<?php

class UsersController extends BaseController {

    /**
     * User Repository
     *
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::check()) {

            $data['login'] = Auth::user()->get;
            $data['users'] = $this->user->all();

            return View::make('users.index', $data);
        }

        return Redirect::route('users.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(Auth::check())
        {
            return Redirect::route('users.index');

        }else{

            return View::make('users.create');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $validation = Validator::make($input, User::$rules);

        $input['password'] = Hash::make($input['password']);
        $input['gravatar_hash'] = md5($input['email']);

        if ($validation->passes())
        {
            $this->user->create($input);

            return Redirect::route('users.index');
        }

        return Redirect::route('users.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->user->findOrFail($id);

        return View::make('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->user->find($id);

        if (is_null($user))
        {
            return Redirect::route('users.index');
        }

        return View::make('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, User::$rules);

        $input['password'] = Hash::make($input['password']);
        $input['gravatar_hash'] = md5($input['email']);

        if ($validation->passes())
        {
            $user = $this->user->find($id);
            $user->update($input);

            return Redirect::route('users.show', $id);
        }

        return Redirect::route('users.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->user->find($id)->delete();

        return Redirect::route('users.index');
    }

    /**
     * Verify data submit by user to login.
     *
     * @return Route
     */

    public function loginverify()
    {
        $input = Input::all();
        $validation = Validator::make($input, User::$loginRules);

        $row = DB::table('users')
            ->whereExists(function($query)
            {
                $input = Input::all();
                $query->select(DB::raw('email'))
                      ->from('users')
                      ->whereRaw('users.email = \''.$input['email'].'\'');
            })
            ->get();

        if (!empty($row[0])) {

            if ($validation->passes() && Hash::check($input['password'], $row[0]->password) )
            {

                Auth::attempt(array('email' => $input['email'], 'password' => $input['password']), true);
                return Redirect::route('synthetweeds.myfeed');

            }
        }

        return Redirect::route('users.login')
            ->withInput()
            ->withErrors($validation)
            ->with('flash', 'There were validation errors.');
    }

    public function login()
    {
        return View::make('users.login');
    }

    public function logout(){

        Auth::logout();
        return Redirect::route('users.login');
    }

    /**
     * Ajax request to check login_id
     *
     * @return Json data
     */

    public function logindata(){

        $data = array();

        if (Auth::check()) {
            $data['login_id'] = Auth::user()->id;
        }

        die(json_encode($data));

    }

}
@extends('layouts.scaffold')

@section('main')

<h1>Voici le Synthetweed</h1>

<p>{{ link_to_route('synthetweeds.myfeed', 'Voir tous mes tweeds') }}</p>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            
				<th>Tweed</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            
				<td>{{ $synthetweed->tweed }}</td>
                <td>{{ link_to_route('synthetweeds.edit', 'Edit', array($synthetweed->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('synthetweeds.destroy', $synthetweed->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
        </tr>
    </tbody>
</table>

@stop
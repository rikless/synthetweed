@extends('layouts.scaffold')

@section('main')

<h1>Modifier un Synthetweed</h1>
{{ Form::model($synthetweed, array('method' => 'PATCH', 'route' => array('synthetweeds.update', $synthetweed->id))) }}
    <ul>
        

        <li>
            {{ Form::label('tweed', 'Tweed:') }}
            {{ Form::text('tweed') }}
        </li>

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('synthetweeds.show', 'Cancel', $synthetweed->id, array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
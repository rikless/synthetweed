@extends('layouts.scaffold')

@section('main')

<h1>Tous les Synthetweeds</h1>

<p>{{ link_to_route('synthetweeds.create', 'Ajouter un synthetweed') }}</p>

@if ($synthetweeds->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Ava</th>
                <th>Username</th>
				<th>Tweed</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($synthetweeds as $synthetweed)
                <tr>
                    <td> <img src="https://secure.gravatar.com/avatar/{{ $synthetweed->user->gravatar_hash }}?s=40"> </td>
                    <td>{{ $synthetweed->user->username }}</td>
					<td>{{ $synthetweed->tweed }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no synthetweeds
@endif

@stop
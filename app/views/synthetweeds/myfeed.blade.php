@extends('layouts.scaffold')

@section('main')

<h1>Mes synthetweeds</h1>

<p>{{ link_to_route('synthetweeds.create', 'Ajouter un synthetweed') }}</p>

@if ($tweeds->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Gravatar</th>
				<th>Synthetweed</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($tweeds as $tweed)
                <tr>
                    <td><img src="https://secure.gravatar.com/avatar/{{ $tweed->user->gravatar_hash }}?s=40"></td>
					<td>{{ $tweed->tweed }}</td>
                    <td>{{ link_to_route('synthetweeds.edit', 'Modifier', array($tweed->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('synthetweeds.destroy', $tweed->id))) }}
                            {{ Form::submit('Supprimer', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no synthetweeds
@endif

@stop
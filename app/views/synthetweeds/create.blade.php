@extends('layouts.scaffold')

@section('main')

<h1>Ajouter un Synthetweed</h1>

{{ Form::open(array('route' => 'synthetweeds.store')) }}
    <ul>

        <li>
            {{ Form::label('tweed', 'Tweed:') }}
            {{ Form::text('tweed') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop



@extends('layouts.scaffold')

@section('main')

<h1>Login page</h1>

{{ Form::open(array('method' => 'POST', 'route' => array('users.loginverify'))) }}
    <ul>
        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('password', 'Password:') }}
            {{ Form::text('password') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

<big>{{ link_to_route('users.create', 'Or ceate new user') }}</big>

@stop



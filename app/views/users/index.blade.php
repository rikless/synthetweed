@extends('layouts.scaffold')

@section('main')

<script type="text/javascript">

window.addEvent('domready', function() {
    var myElement = $$('.content');
    var log_user_id;

    var myRequest = new Request.JSON({
        url: 'users/logindata',
        method: 'get',
        onRequest: function(){
            myElement.set('text', 'loading...');
        },
        onSuccess: function(responseText){
            myElement.set('text', 'responseText');
            log_user_id = responseText.login_id;
        },
        onFailure: function(){
            myElement.set('text', 'Sorry, your request failed :(');
        }
    });

    myRequest.send();

    
    $$('table td.follow a').addEvent('click', function(){
        var follow = this.get('data-id');
        var td = this;
        var AddFollow = new Request.JSON({
            url: 'follows/followme',
            method: 'post',
            data : { user_id : follow, follow_id : log_user_id},
            onRequest: function(){
                td.set('text', 'loading...');
            },
            onSuccess: function(responseText){
                console.log(responseText);
                td.set('text', 'Yeah!');
                
            },
            onFailure: function(){
                td.set('text', 'Sorry, your request failed :(');
            }
     
        });

    AddFollow.send();
        
    });
});
    
</script>

<h1>Synthesios users</h1>

@if (isset($login))
<p>{{ link_to_route('users.create', 'Add new user') }}</p>
@endif
@if ($users->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Username</th>
				
				<th>Gravatar</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($users as $user)
                <tr>
                    				
					<td><img src="https://secure.gravatar.com/avatar/{{ $user->gravatar_hash }}?s=40"></td>
                    <td>{{ $user->username }}</td>  
                    <td class="follow" data-auth="{{ $login }}"><a href="#" class="btn btn-info" data-id="{{ $user->id }}">Follow</a></td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no users
@endif

@stop
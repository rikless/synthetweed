<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
        <script src="//ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools-yui-compressed.js"></script>
        <style>
            table form { margin-bottom: 0; }
            form ul { margin-left: 0; list-style: none; }
            .error { color: red; font-style: italic; }
            body { padding-top: 20px; }
        </style>
    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <ul class="nav">
                                <li>{{ link_to_route('users.index', 'Synthusers') }}</li>
                                <li>{{ link_to_route('synthetweeds.myfeed', 'Voir mes Tweeds') }}</li>
                                <li>{{ link_to_route('synthetweeds.index', 'Tous les Tweeds') }}</li>
                                <li>{{ link_to_route('follows.feed', 'Flux d\'actu') }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if (Session::has('message'))
                <div class="flash alert">
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif

            @yield('main')
        </div>

    </body>

</html>
@extends('layouts.scaffold')

@section('main')

<h1>Flux d'actualité</h1>

<p>{{ link_to_route('users.index', 'Trouver des utilisateurs à suivre') }}</p>

@if ($follows->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Username</th>
				<th>Gravatar</th>
                <th>Tweeds</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($follows as $follow)
                @foreach ($follow->user->tweeds as $message)
                    <tr>
                        <td>{{$follow->user->username}}
                        <td><img src="https://secure.gravatar.com/avatar/{{$follow->user->gravatar_hash }}?s=40"></td>
                        <td>{{ $message->tweed }}</td>
                    </tr>
                @endforeach
            @endforeach
            
        </tbody>
    </table>
@else
    Vous ne suivez personne, ou aucune des personne que vous suivez n'a publié de message
@endif

@stop
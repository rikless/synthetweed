@extends('layouts.scaffold')

@section('main')

<h1>Create Follow</h1>

{{ Form::open(array('route' => 'follows.store')) }}
    <ul>
        <li>
            {{ Form::label('user_id', 'User_id:') }}
            {{ Form::text('user_id') }}
        </li>

        <li>
            {{ Form::label('follow_id', 'Follow_id:') }}
            {{ Form::text('follow_id') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop



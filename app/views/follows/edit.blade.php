@extends('layouts.scaffold')

@section('main')

<h1>Edit Follow</h1>
{{ Form::model($follow, array('method' => 'PATCH', 'route' => array('follows.update', $follow->id))) }}
    <ul>
        <li>
            {{ Form::label('user_id', 'User_id:') }}
            {{ Form::text('user_id') }}
        </li>

        <li>
            {{ Form::label('follow_id', 'Follow_id:') }}
            {{ Form::text('follow_id') }}
        </li>

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('follows.show', 'Cancel', $follow->id, array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
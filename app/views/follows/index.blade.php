@extends('layouts.scaffold')

@section('main')

<h1>All Follows</h1>

<p>{{ link_to_route('follows.create', 'Add new follow') }}</p>

@if ($follows->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>User_id</th>
				<th>Follow_id</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($follows as $follow)
                <tr>
                    <td>{{ $follow->user_id }}</td>
					<td>{{ $follow->follow_id }}</td>
                    <td>{{ link_to_route('follows.edit', 'Edit', array($follow->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('follows.destroy', $follow->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no follows
@endif

@stop
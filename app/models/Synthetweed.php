<?php

class Synthetweed extends Eloquent {
    protected $guarded = array();

    public static $rules = array(
		'user_id' => 'required',
		'tweed' => 'required'
	);

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}
}
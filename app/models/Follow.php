<?php

class Follow extends Eloquent {
    protected $guarded = array();

    public static $rules = array(
		'follow_id' => 'required',
		'user_id' => 'required'
	);


	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}
}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('users/login', array('as' => 'users.login', 'uses' => 'UsersController@login'));

Route::any('follows/followme', array('as' => 'follows.followme', 'uses' => 'FollowsController@followme'));

Route::get('users/logindata', array('as' => 'users.logindata', 'uses' => 'UsersController@logindata'));

Route::post('users/loginverify', array('as' => 'users.loginverify', 'uses' => 'UsersController@loginverify'));

Route::get('users/logout', array('as' => 'users.logout', 'uses' => 'UsersController@logout'));

Route::group(array('before' => 'auth'), function() {

	Route::get('synthetweeds/myfeed', array('as' => 'synthetweeds.myfeed', 'uses' => 'SynthetweedsController@myfeed'));

	Route::get('synthetweeds/create', array('as' => 'synthetweeds.create', 'uses' => 'SynthetweedsController@create'));

	Route::get('follows/feed', array('as' => 'follows.feed', 'uses' => 'FollowsController@feed'));

});

Route::resource('users', 'UsersController');

Route::resource('synthetweeds', 'SynthetweedsController', ['only' => ['index','store','whatever', 'destroy', 'update', 'edit', 'show', 'store']]);

Route::resource('follows', 'FollowsController');